const mainArr = [];

function requestData(method, action) {
    const request = new XMLHttpRequest();
    request.open(method, action);
    request.send();

    const parse = response => JSON.parse(response);

    request.addEventListener('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            mainArr.push(...parse(request.response).children);
        }
    });
}

requestData('GET', 'request/first.json');
requestData('GET', 'request/second.json');
